package com.lb.controller.criteria;

import lombok.Data;

@Data
public class WinningUserCriteria {

    String phone;

    String nick;

    String prizeName;
}
