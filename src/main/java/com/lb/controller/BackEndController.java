package com.lb.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.R;
import com.lb.controller.criteria.WinningUserCriteria;
import com.lb.model.DrawCode;
import com.lb.model.Prize;
import com.lb.model.PrizeCode;
import com.lb.model.WinningUser;
import com.lb.service.DrawCodeService;
import com.lb.service.PrizeCodeService;
import com.lb.service.PrizeService;
import com.lb.service.WinningUserService;
import com.lb.util.common.response.ResponseObj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("backend/")
public class BackEndController {

    @Autowired
    DrawCodeService drawCodeService;

    @Autowired
    WinningUserService winningUserService;

    @Autowired
    PrizeService prizeService;

    @Autowired
    PrizeCodeService prizeCodeService;

    //获取抽奖码
    @RequestMapping(value = "getdrawcode",method = RequestMethod.GET)
    public ResponseObj getDrawCode(@RequestParam Integer count,@RequestParam Integer number){
        if(count>5){
            count=5;
        }
        if(number>5){
            number=5;
        }
        List<DrawCode> list=new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, +1);//+1今天的时间加一天
        Date nextDay = calendar.getTime();
        for (int i=0;i<count;i++){
            String uuid=drawCodeService.getUUid();
            DrawCode drawCode=new DrawCode(uuid,new Date(),number,nextDay);
            Boolean  hasRound=true;
            do {
                try {
                    drawCodeService.save(drawCode);
                    hasRound=false;
                    list.add(drawCode);
                } catch (DuplicateKeyException e) {
                    uuid=drawCodeService.getUUid();
                    drawCode.setCode(uuid);
                }
            }while (hasRound);
        }
        return ResponseObj.createSuccessResponse(list);
    }

    //奖品列表信息
    @RequestMapping(value = "getprizelist",method = RequestMethod.GET)
    public ResponseObj getPrizeList(){
        List<Prize> list=prizeService.list();
        return ResponseObj.createSuccessResponse(list);
    }

    //验证中奖码
    @RequestMapping(value = "getprizecode",method = RequestMethod.GET)
    public ResponseObj getPrizeCode(@RequestParam String prizeCode){


        PrizeCode entity= prizeCodeService.getOne(new QueryWrapper<PrizeCode>().eq("code",prizeCode));

        if(entity==null){
            return ResponseObj.createResponse(-1,"没有该中奖码");
        }

        long overdue=new Date().getTime()-entity.getCreateTime().getTime();

        if (overdue>=86400000){
            return ResponseObj.createResponse(-1,"该中奖码已过期，兑奖有效期为24小时");
    }

        if(entity.getHasUsed().equals(1)){
            return ResponseObj.createResponse(-1,"该中奖码已使用");
        }
        return ResponseObj.createSuccessResponse(entity);
    }

    //修改该中奖码为已经使用
    @RequestMapping(value = "updateusedprizecode",method = RequestMethod.GET)
    public ResponseObj updateUsedPrizeCode(@RequestParam String code){
        PrizeCode prizeCode=prizeCodeService.getOne(new QueryWrapper<PrizeCode>().eq("code",code));
        if(prizeCode==null){
            return ResponseObj.createResponse(-1,"没有该中奖码");
        }
        long overdue=new Date().getTime()-prizeCode.getCreateTime().getTime();
        if (overdue>=86400000){
            prizeCode.setHasUsed(1);
            prizeCodeService.update(prizeCode,new QueryWrapper<PrizeCode>().eq("code",code));
            return ResponseObj.createResponse(-1,"该中奖码已过期，兑奖有效期为24小时");
        }
        prizeCode.setHasUsed(1);
        prizeCodeService.update(prizeCode,new QueryWrapper<PrizeCode>().eq("code",code));
        return ResponseObj.createSuccessResponse();
    }

   /* public ResponseObj prizeCode(String code){
        PrizeCode prizeCode=prizeCodeService.getOne(new QueryWrapper<PrizeCode>().eq("code",code));
        if(prizeCode==null)ResponseObj.createResponse(-1,"没有该中奖码");
        return
    }*/


    //保存中奖人信息
    @RequestMapping(value = "savewinners",method = RequestMethod.POST)
    public ResponseObj saveWinners(@RequestBody WinningUserCriteria param){
        Integer nextId=winningUserService.getNextId();
        WinningUser user=new WinningUser(nextId,param.getPhone(),param.getNick(),new Date(),param.getPrizeName());
        winningUserService.save(user);
        return ResponseObj.createSuccessResponse();
    }
}
