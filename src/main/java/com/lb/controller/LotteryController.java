package com.lb.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.lb.model.*;
import com.lb.service.*;
import com.lb.util.common.response.ResponseObj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class LotteryController {


    @Autowired
    PrizeService prizeService;

    @Autowired
    WinningUserService winningUserService;

    @Autowired
    AddrService addrService;

    @Autowired
    DrawCodeService drawCodeService;

    @Autowired
    PrizeCodeService prizeCodeService;

    @Autowired
    PrizeIpService prizeIpService;


    //用抽奖码兑换抽奖次数
    @RequestMapping(value = "getexchangetimes",method = RequestMethod.GET)
    @Transactional
    public ResponseObj getExchangeTimes(HttpServletRequest request, @RequestParam String code){
        String ip =request.getRemoteAddr();
        Addr addr=addrService.getOne(new QueryWrapper<Addr>().eq("ip",ip));

        DrawCode drawCode=drawCodeService.getOne(new QueryWrapper<DrawCode>().eq("code",code));
       /* if(addr==null){
            addr=new Addr(ip,new Date(),drawCode.getTimes());
        }*///目前代码经过修改了之后，这一块不可能等于null
        if(drawCode==null){
            return ResponseObj.createResponse(-1,"没有该抽奖码");
        }
        if(new Date().getTime()>=drawCode.getOverdue().getTime()){
            drawCode.setHasExpire(1);
            drawCodeService.update(drawCode,new QueryWrapper<DrawCode>().eq("code",code));
            return ResponseObj.createResponse(-1,"该抽奖码已过期");
        }
        if(drawCode.getHasExpire().equals(1)){
            return ResponseObj.createResponse(-1,"该抽奖码已使用");
        }
        Integer times=addr.getDuramax()+drawCode.getTimes();
        addr.setDuramax(times);
        addrService.update(addr,new QueryWrapper<Addr>().eq("ip",addr.getIp()));
        drawCode.setHasExpire(1);
        drawCodeService.update(drawCode,new QueryWrapper<DrawCode>().eq("code",drawCode.getCode()));

        return ResponseObj.createSuccessResponse("兑换成功，您拥有"+times+"次抽奖次数");
    }


    //进行抽奖
    @RequestMapping(value = "drawlottery",method = RequestMethod.GET)
    @Transactional
    public ResponseObj DrawLottery(HttpServletRequest request){
        String ip =request.getRemoteAddr();
        Addr addr=addrService.getOne(new QueryWrapper<Addr>().eq("ip",ip));
        if(addr==null){
            addr=new Addr(ip,new Date(),0);
            addrService.save(addr);
            return ResponseObj.createResponse(-1,"您已经没有了抽奖次数");
        }
        if(addr.getDuramax()<=0){
            return ResponseObj.createResponse(-1,"您已经没有了抽奖次数");
        }
        int num= (int) (Math.random()*100)+1;
        int min=0,max=0;
        List<Prize> prizes=prizeService.list();
        Prize selectPrize=null;
        for (Prize prize:prizes){
            if (prize.getRatio()==0){
                continue;
            }
            max+=prize.getRatio();
            //如果max==1
            if(num>min&&num<=max){
                //筛选中奖奖品
                selectPrize=prize;
                break;
            }else {
                min+=prize.getRatio();
            }
        }
        if(selectPrize==null){
            return ResponseObj.createResponse(-2,"所有奖品概率没达到100%，中奖值范围超出");
        }
        addr.setDuramax(addr.getDuramax()-1);
        addrService.update(addr,new UpdateWrapper<Addr>().eq("ip",addr.getIp()));
        String uuid=prizeCodeService.uuid();
        PrizeCode prizeCode=new PrizeCode(uuid,selectPrize.getId(),selectPrize.getPrizeName(),new Date());
        Boolean  hasRound=true;
        do {
            try {
                prizeCodeService.save(prizeCode);
                hasRound=false;
            } catch (DuplicateKeyException e) {
                uuid=prizeCodeService.uuid();
                prizeCode.setCode(uuid);
            }
        }while (hasRound);
        PrizeIp prizeIp=new PrizeIp();
        prizeIp.setIp(ip);
        prizeIp.setCreateTime(new Date());
        prizeIp.setPrizeName(selectPrize.getPrizeName());
        prizeIp.setPrizeCode(prizeCode.getCode());
        prizeIpService.save(prizeIp);
        return ResponseObj.createSuccessResponse(prizeCode);
    }

    @RequestMapping(value = "save_addr",method = RequestMethod.GET)
    public void saveAddr(HttpServletRequest httpServlet){
        String ip=httpServlet.getRemoteAddr();
        Addr addr=addrService.getOne(new QueryWrapper<Addr>().eq("ip",ip));
        if(addr==null) {
            addr = new Addr(ip,new Date(),0);
            addrService.save(addr);
        }
    }

    //列出所有中奖者信息
    @RequestMapping(value = "getlistwinninguser",method = RequestMethod.GET)
    public ResponseObj getListWinningUser(){
        List<WinningUser> list=winningUserService.list();


        return ResponseObj.createSuccessResponse(list);
    }

    //列出该ip中奖信息。
    @RequestMapping(value = "getprizebyid",method = RequestMethod.GET)
    public ResponseObj getPrizeByIp(HttpServletRequest request){
        String ip =request.getRemoteAddr();
        List<PrizeIp> prizeIpList=prizeIpService.list(new QueryWrapper<PrizeIp>().eq("ip",ip));
        return ResponseObj.createSuccessResponse(prizeIpList);

    }

}
