package com.lb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lb.model.PrizeCode;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
public interface PrizeCodeMapper extends BaseMapper<PrizeCode> {

    @Select("select left(replace(uuid(), '-', ''),12);")
    String getUUid();
}
