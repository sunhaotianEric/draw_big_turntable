package com.lb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lb.model.WinningUser;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
public interface WinningUserMapper extends BaseMapper<WinningUser> {

    @Select("select Max(id) from winning_user")
    Integer getNextId();
}
