package com.lb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lb.model.PrizeIp;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PrizeIpMapper extends BaseMapper<PrizeIp> {
}
