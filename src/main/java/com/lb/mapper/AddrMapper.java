package com.lb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lb.model.Addr;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
public interface AddrMapper extends BaseMapper<Addr> {


    @Select("select * from Addr ")
    List<Addr> getone();

    @Select("select*from addr where duramax>0")
    List<Addr> getDuramax();

}
