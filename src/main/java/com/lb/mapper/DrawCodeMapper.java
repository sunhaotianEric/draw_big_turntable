package com.lb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lb.model.DrawCode;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
public interface DrawCodeMapper extends BaseMapper<DrawCode> {


    @Select("select left(uuid(),8) ")
    String uuid();
}
