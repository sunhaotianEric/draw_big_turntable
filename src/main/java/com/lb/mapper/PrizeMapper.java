package com.lb.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lb.model.Prize;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Mapper
public interface PrizeMapper extends BaseMapper<Prize> {
}
