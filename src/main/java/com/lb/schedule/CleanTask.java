package com.lb.schedule;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.lb.model.Addr;
import com.lb.model.PrizeIp;
import com.lb.service.AddrService;
import com.lb.service.PrizeIpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class CleanTask {

    @Autowired
    AddrService addrService;

    @Autowired
    PrizeIpService prizeIpService;


    //每日凌晨12点将还有抽奖次数的IP地址全部设为0
    @Scheduled(cron = "0 0 0 * * ? ")
    private void clean()throws  Exception{
        addrService.remove(new UpdateWrapper<Addr>().eq("duramax",0));
        List<Addr> addrs =addrService.getDuramax();
        for (Addr addr:addrs) {
            Date now = new Date();
            if(now.getTime()>=addr.getCreateTime().getTime()){
                addr.setDuramax(0);
                boolean flag=addrService.update(addr,new QueryWrapper<Addr>().eq("ip",addr.getIp()));
                if (!flag){
                    throw new Exception("更新ip为:"+addr.getIp()+"的用户时遇到未知错误");
                }
            }
        }

        //清除用户中奖码列表过期的
        List<PrizeIp> prizeIpList=prizeIpService.list();
            for (PrizeIp prizeIp:prizeIpList){
                Long today=new Date().getTime();
            Long hasOverde=prizeIp.getCreateTime().getTime()-today;
            if(hasOverde>172800) {//计算是否超过两天
                prizeIpService.removeById(prizeIp.getId());
            }
        }
    }
}
