package com.lb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.model.WinningUser;

public interface WinningUserService extends IService<WinningUser> {

    Integer getNextId();
}
