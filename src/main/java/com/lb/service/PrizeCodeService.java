package com.lb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.model.PrizeCode;

public interface PrizeCodeService extends IService<PrizeCode> {

    String uuid();
}
