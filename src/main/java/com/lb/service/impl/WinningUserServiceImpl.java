package com.lb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.mapper.WinningUserMapper;
import com.lb.model.WinningUser;
import com.lb.service.WinningUserService;
import org.springframework.stereotype.Service;

@Service
public class WinningUserServiceImpl extends ServiceImpl<WinningUserMapper, WinningUser> implements WinningUserService {


    @Override
    public Integer getNextId() {
        Integer nextId=baseMapper.getNextId()+1;
        return nextId;
    }
}
