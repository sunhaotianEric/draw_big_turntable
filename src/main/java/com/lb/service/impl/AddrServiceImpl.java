package com.lb.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.mapper.AddrMapper;
import com.lb.model.Addr;
import com.lb.service.AddrService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddrServiceImpl extends ServiceImpl<AddrMapper, Addr> implements AddrService {


    @Override
    public List<Addr> getDuramax() {
        return baseMapper.getDuramax();
    }
}
