package com.lb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.mapper.PrizeIpMapper;
import com.lb.model.PrizeIp;
import com.lb.service.PrizeIpService;
import org.springframework.stereotype.Service;

@Service
public class PrizeIpServiceImpl extends ServiceImpl<PrizeIpMapper, PrizeIp> implements PrizeIpService {
}
