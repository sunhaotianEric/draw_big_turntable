package com.lb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.mapper.DrawCodeMapper;
import com.lb.model.DrawCode;
import com.lb.service.DrawCodeService;
import org.springframework.stereotype.Service;

@Service
public class DrawCodeServiceImpl extends ServiceImpl<DrawCodeMapper, DrawCode> implements DrawCodeService {

    @Override
    public String getUUid() {
        return baseMapper.uuid();
    }
}
