package com.lb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.mapper.PrizeCodeMapper;
import com.lb.model.PrizeCode;
import com.lb.service.PrizeCodeService;
import org.springframework.stereotype.Service;

@Service
public class PrizeCodeServiceImpl extends ServiceImpl<PrizeCodeMapper, PrizeCode> implements PrizeCodeService {
    @Override
    public String uuid() {
        return baseMapper.getUUid();
    }
}
