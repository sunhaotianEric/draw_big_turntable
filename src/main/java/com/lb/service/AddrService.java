package com.lb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.model.Addr;

import java.util.List;

public interface AddrService extends IService<Addr> {
    List<Addr> getDuramax();
}
