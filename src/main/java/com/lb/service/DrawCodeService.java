package com.lb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.model.DrawCode;

public interface DrawCodeService extends IService<DrawCode> {

    String getUUid();
}
