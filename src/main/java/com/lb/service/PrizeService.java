package com.lb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.model.Prize;

public interface PrizeService extends IService<Prize> {
}
