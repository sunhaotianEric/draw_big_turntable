package com.lb.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("prize")
public class Prize {

    //奖品id
    Integer id;

    //奖品名称
    String prizeName;

    //中奖次数
    Integer times;

    //中奖概率 百分制100%
    Integer ratio;



}
