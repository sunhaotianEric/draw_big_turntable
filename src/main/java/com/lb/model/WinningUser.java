package com.lb.model;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("winning_user")
public class WinningUser {

    Integer id;

    String phone;

    String nick;

    Date winingTime;

    String prizeName;

    public WinningUser(Integer id,String phone, String nick, Date winingTime, String prizeName) {
        this.id=id;
        this.phone = phone;
        this.nick = nick;
        this.winingTime = winingTime;
        this.prizeName = prizeName;
    }

    public WinningUser() {
    }
}
