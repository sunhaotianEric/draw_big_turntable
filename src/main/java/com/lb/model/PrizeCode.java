package com.lb.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("prize_code")
public class PrizeCode {

    String code;

    Integer prizeId;

    String prizeName;

    Date createTime;

    Integer hasUsed;

    public PrizeCode() {
    }

    public PrizeCode(String code, Integer prizeId, String prizeName, Date createTime) {
        this.code = code;
        this.prizeId = prizeId;
        this.prizeName = prizeName;
        this.createTime = createTime;
        this.hasUsed = 0;
    }
}
