package com.lb.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("draw_code")
public class DrawCode {

    String code;

    Date createTime;

    Integer times;

    Date overdue;

    Integer hasExpire;

    public DrawCode() {
    }

    public DrawCode(String code, Date createTime, Integer times, Date overdue) {
        this.code = code;
        this.createTime = createTime;
        this.times = times;
        this.overdue = overdue;
        this.hasExpire=0;
    }

}
