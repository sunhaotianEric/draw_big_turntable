package com.lb.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("addr")
public class Addr {


    String ip;

    Date createTime;

    Integer duramax;

    public Addr(String ip, Date createTime, Integer duramax) {
        this.ip = ip;
        this.createTime = createTime;
        this.duramax = duramax;
    }
}
