package com.lb.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@TableName("prize_ip")
@Data
public class PrizeIp {

    @TableId(value = "id",type = IdType.AUTO)
    Integer id;

    String ip;

    String prizeCode;

    String prizeName;

    Date createTime;
}
